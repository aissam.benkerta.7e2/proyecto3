// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDl0gTwR_nvFYTcZlOeJ1YIVNX0M_JDy_w",
    authDomain: "ntfs-93d14.firebaseapp.com",
    projectId: "ntfs-93d14",
    storageBucket: "ntfs-93d14.appspot.com",
    messagingSenderId: "241050894916",
    appId: "1:241050894916:web:aa06e05d20f40f9ba23211"
  },
  mapBoxToken: 'pk.eyJ1IjoiYWlzc2FtaXRiIiwiYSI6ImNsMzc3YTBpYjBlMHIzcm8wOG9kNmQ0MmsifQ.bEAOEerM6JYvI0htHtLsfw'
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

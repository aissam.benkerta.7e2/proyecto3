import { Injectable } from '@angular/core';
import { CanLoad, Route, Router, UrlSegment, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {
  
  constructor(public authService: AuthenticationService,
    public router: Router){ }
 
  canLoad(): boolean {
    const result = this.authService.isLoggedIn
    if(!result){
      this.router.navigateByUrl('/login');
    }
    return this.authService.isLoggedIn;
  }
 
}

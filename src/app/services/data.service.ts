import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { Cliente, Producto } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private firestore: AngularFirestore, private router: Router,private http: HttpClient) { }
  
  createCliente(cliente:Cliente ){
    return this.firestore.collection('clientes').add(cliente);
  }
 
  getClientes() {
    return this.firestore.collection('clientes').snapshotChanges();
  }
 
  getCliente(id) {
    return this.firestore.collection('clientes').doc(id).valueChanges();
  }

  updateCliente(id, cliente: Cliente) {
    this.firestore.collection('clientes').doc(id).update(cliente)
      .then(() => {
        this.router.navigate(['clientes']);
      }).catch(error => console.log(error));
  }
 
  deleteCliente(id) {
    this.firestore.doc('clientes/'+id).delete();
  }

  createProductos(producto:Producto ){
    return this.firestore.collection('productos').add(producto);
  }
 
  getProductos() {
    return this.firestore.collection('productos').snapshotChanges();
  }
 
  getProducto(id) {
    return this.firestore.collection('productos').doc(id).valueChanges();
  }

  updateProducto(id, producto: Producto) {
    this.firestore.collection('productos').doc(id).update(producto)
      .then(() => {
        this.router.navigate(['productos']);
      }).catch(error => console.log(error));
  }
 
  deleteProducto(id) {
    this.firestore.doc('productos/'+id).delete();
  }
  getUbicacion(direccion) {
    return this.http.get<any>(`https://api.mapbox.com/geocoding/v5/mapbox.places/${direccion}.json?access_token=pk.eyJ1IjoiYWlzc2FtaXRiIiwiYSI6ImNsMzc3YTBpYjBlMHIzcm8wOG9kNmQ0MmsifQ.bEAOEerM6JYvI0htHtLsfw`);
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AproductoPageRoutingModule } from './aproducto-routing.module';

import { AproductoPage } from './aproducto.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AproductoPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [AproductoPage]
})
export class AproductoPageModule {}

import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-aproducto',
  templateUrl: './aproducto.page.html',
  styleUrls: ['./aproducto.page.scss'],
})
export class AproductoPage implements OnInit {

  productoForm: FormGroup;
  constructor(private dataService: DataService,
    public formBuilder: FormBuilder,   
    private router: Router) { }

  ngOnInit() {
    this.productoForm = this.formBuilder.group({
      nombre: [''],
      categoria: [''],
      precio: ['']
    })
  }
  onSubmit() {
    if (!this.productoForm.valid) {
      return false;
    } else {
      this.dataService.createProductos(this.productoForm.value)
      .then(() => {
        this.productoForm.reset();
        this.router.navigate(['productos']);
      }).catch((err) => {
        console.log(err)
      });
    }
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AproductoPage } from './aproducto.page';

const routes: Routes = [
  {
    path: '',
    component: AproductoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AproductoPageRoutingModule {}

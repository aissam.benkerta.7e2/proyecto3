import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  constructor(private router: Router, private authService: AuthenticationService,private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email:[''],
      password:[''],
    })
  }

  onSubmit(){
    if(!this.loginForm.valid){
      return false;
    } else {
      this.login(this.loginForm.value.email, this.loginForm.value.password)
    }
  }

  login(email, password) {
    console.log(email)
   this.authService.signIn(email, password)
     .then((res) => {
       this.authService.getUser(email).subscribe(
         res=>{
           localStorage.setItem('admin', res['admin'].toString())
           localStorage.setItem('authenticated', 'true');
           if(res['admin']){
             this.router.navigateByUrl('/admin');
           }else{
             this.router.navigateByUrl('/cproductos');
           }
         }
       )
     }).catch((error) => {
       window.alert(error.message)
     })
 }


}

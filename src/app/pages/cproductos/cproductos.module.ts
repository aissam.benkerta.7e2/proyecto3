import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CproductosPageRoutingModule } from './cproductos-routing.module';

import { CproductosPage } from './cproductos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CproductosPageRoutingModule
  ],
  declarations: [CproductosPage]
})
export class CproductosPageModule {}

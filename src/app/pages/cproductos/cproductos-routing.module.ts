import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CproductosPage } from './cproductos.page';

const routes: Routes = [
  {
    path: '',
    component: CproductosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CproductosPageRoutingModule {}

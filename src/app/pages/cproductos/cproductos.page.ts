import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Producto } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-cproductos',
  templateUrl: './cproductos.page.html',
  styleUrls: ['./cproductos.page.scss'],
})
export class CproductosPage implements OnInit {

  productos = [];
  msg: string;
  constructor(private dataService: DataService,private router: Router) { }

  ngOnInit() {
    this.dataService.getProductos().subscribe(
      res => {
        this.productos = res.map((item) => {
          return {
            id: item.payload.doc.id,
            ... item.payload.doc.data() as Producto
          };
        })
      }
    );
    if(this.productos==null){
      this.msg = 'No hay Ningun Producto Disponible'
      console.log(this.msg)
    }
 
  }
}

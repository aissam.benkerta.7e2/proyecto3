import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MapaclientePage } from './mapacliente.page';

const routes: Routes = [
  {
    path: '',
    component: MapaclientePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MapaclientePageRoutingModule {}

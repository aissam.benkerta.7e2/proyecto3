import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { environment } from 'src/environments/environment';
declare var mapboxgl: any;


@Component({
  selector: 'app-mapacliente',
  templateUrl: './mapacliente.page.html',
  styleUrls: ['./mapacliente.page.scss'],
})

export class MapaclientePage implements OnInit {

  latitud: any;
  longitud: any;
  mapa : any;
  direccion : String;
  localicacion: any 
  constructor(private dataService: DataService,private activatedRoute: ActivatedRoute) { 
    this.activatedRoute.queryParams.subscribe(params => {
      this.direccion = params.localicacion;
        this.dataService.getUbicacion(this.direccion).subscribe(
          resp => {
            this.mapa = resp
            this.longitud = this.mapa.features[0].center[0]
            this.latitud = this.mapa.features[0].center[1]
            console.log(this.longitud)
            console.log(this.latitud)
            mapboxgl.accessToken = environment.mapBoxToken
            var map = new mapboxgl.Map({
              container: 'map',
              style: 'mapbox://styles/mapbox/streets-v11',
              center: [this.longitud, this.latitud],
              zoom: 10
            });

            map.addControl(new mapboxgl.NavigationControl());
            var marker = new mapboxgl.Marker({})
            .setLngLat([this.longitud, this.latitud])
            .setPopup(new mapboxgl.Popup().setHTML(`<h5>${this.direccion} </h5>`))
            .addTo(map);
            
            map.on('load', () => {
              map.resize();
            });
          }
        );
        
    });
  }

  ngOnInit() {
    
  }

}

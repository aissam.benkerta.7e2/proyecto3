import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapaclientePageRoutingModule } from './mapacliente-routing.module';

import { MapaclientePage } from './mapacliente.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MapaclientePageRoutingModule
  ],
  declarations: [MapaclientePage]
})
export class MapaclientePageModule {}

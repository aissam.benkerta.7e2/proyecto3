import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmailComposer } from '@awesome-cordova-plugins/email-composer/ngx';
import { AlertController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';
import { Producto } from '../../interfaces/interfaces';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.page.html',
  styleUrls: ['./productos.page.scss'],
})
export class ProductosPage implements OnInit {
  productos = [];
  constructor(private dataService: DataService,private router: Router,private emailComposer: EmailComposer,public alertController: AlertController) { }

  ngOnInit() {
    this.dataService.getProductos().subscribe(
      res => {
        this.productos = res.map((item) => {
          return {
            id: item.payload.doc.id,
            ... item.payload.doc.data() as Producto
          };
        })
      }
    );
 
  }
  deleteProducto(id){
    if (window.confirm('Seguro quieres borrar este cliente?')) {
      this.dataService.deleteProducto(id)
    }
  }

  editar(){
    this.router.navigate(["eproducto"])
  }
  anadir(){
    this.router.navigate(["aproducto"])
  }

  comprar(){
    let getUser = localStorage.getItem('user');
    let user = JSON.parse(getUser);
    let email = {
      to: user.email,
      attachments: ['../../../assets/icon/logo.png'],
      subject: 'Compra Confirmada',
      body: 'Enhorabuena por comprar tu NFT suerte !!', isHtml: true
    }
    this.emailComposer.open(email);
    this.presentAlert()
    
  }
  async presentAlert() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Compra Confirmada',
      message: 'La compra de tu NFT a sido confirmada!!',
      buttons: ['OK']
    });

    await alert.present();
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AclientePageRoutingModule } from './acliente-routing.module';

import { AclientePage } from './acliente.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AclientePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [AclientePage]
})
export class AclientePageModule {}

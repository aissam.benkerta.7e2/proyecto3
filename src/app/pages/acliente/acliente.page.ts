import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-acliente',
  templateUrl: './acliente.page.html',
  styleUrls: ['./acliente.page.scss'],
})
export class AclientePage implements OnInit {

  clienteForm: FormGroup;
  constructor(private dataService: DataService,
    public formBuilder: FormBuilder,   
    private router: Router) { }

  ngOnInit() {
    this.clienteForm = this.formBuilder.group({
      usuario: [''],
      email: [''],
      contrasenya: [''],
      localicacion: ['']
    })
  }
  onSubmit() {
    if (!this.clienteForm.valid) {
      return false;
    } else {
      this.dataService.createCliente(this.clienteForm.value)
      .then(() => {
        this.clienteForm.reset();
        this.router.navigate(['clientes']);
      }).catch((err) => {
        console.log(err)
      });
    }
  }
  
 

}

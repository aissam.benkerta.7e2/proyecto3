import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AclientePage } from './acliente.page';

const routes: Routes = [
  {
    path: '',
    component: AclientePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AclientePageRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
@Component({
  selector: 'app-eproducto',
  templateUrl: './eproducto.page.html',
  styleUrls: ['./eproducto.page.scss'],
})
export class EproductoPage implements OnInit {
  editForm: FormGroup;
  id: String; 
  constructor(private dataService: DataService, private activatedRoute: ActivatedRoute,
    private router: Router,public formBuilder: FormBuilder) { 
      this.id = this.activatedRoute.snapshot.paramMap.get('id');
      this.dataService.getProducto(this.id).subscribe(
        res => {
        this.editForm = this.formBuilder.group({
          usuario: [res['usuario']],
          email: [res['email']],
          contrasenya: [res['contrasenya']]
        })
      });
   
    }

  ngOnInit() {
  }

}

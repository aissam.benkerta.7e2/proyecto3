import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-ecliente',
  templateUrl: './ecliente.page.html',
  styleUrls: ['./ecliente.page.scss'],
})
export class EclientePage implements OnInit {
  editForm: FormGroup;
  id: String; 
  constructor(private dataService: DataService, private activatedRoute: ActivatedRoute,
    private router: Router,public formBuilder: FormBuilder) { 
      this.id = this.activatedRoute.snapshot.paramMap.get('id');
      this.dataService.getCliente(this.id).subscribe(
        res => {
        this.editForm = this.formBuilder.group({
          usuario: [res['usuario']],
          email: [res['email']],
          contrasenya: [res['contrasenya']]
        })
      });
   
    }

  ngOnInit() {
    this.editForm = this.formBuilder.group({
      usuario: [''],
      email: [''],
      contrasenya: ['']
    })
  }
  onSubmit() {
    this.dataService.updateCliente(this.id, this.editForm.value);
  }
 

}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm: FormGroup;
  
  constructor(private router: Router, private authService: AuthenticationService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      username: [''],
      email:[''],
      password:[''],
      admin:['']
    })
  }

  onSubmit(){
    if(!this.registerForm.valid){
      return false;
    } else {
      this.signUp(this.registerForm.value.username, this.registerForm.value.email,this.registerForm.value.password,this.registerForm.value.admin)
    }
  }

  signUp(userName, email, password, admin){
    this.authService.registerUser(userName, email, password, admin)     
    .then((res) => {
      this.authService.signIn(email, password);

      admin == null ? admin = false : admin = true;
      localStorage.setItem('admin', admin)
      localStorage.setItem('authenticated','true');
      if(admin){
        this.router.navigate(['/admin']);
      } else{
        this.router.navigate(['/cproductos'])
      }
    }).catch((error) => {
      window.alert(error.message)
    })
  }
}

import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { Cliente } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.page.html',
  styleUrls: ['./clientes.page.scss'],
})
export class ClientesPage implements OnInit {
  clientes = [];
  constructor(private dataService: DataService,private router: Router) { }

  ngOnInit() {
    this.dataService.getClientes().subscribe(
      res => {
        this.clientes = res.map((item) => {
          return {
            id: item.payload.doc.id,
            ... item.payload.doc.data() as Cliente
          };
        })
      }
    );
 
  }
  deleteCliente(id){
    console.log(id)
    if (window.confirm('Seguro quieres borrar este cliente?')) {
      this.dataService.deleteCliente(id)
    }
  }

  anadir(){
    this.router.navigate(["acliente"])
  }

  verMapa(localicacion){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        localicacion: localicacion,
      }
    }
      this.router.navigate(['mapacliente'], navigationExtras)
  }

}

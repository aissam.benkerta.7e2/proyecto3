import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'admin',
    loadChildren: () => import('./pages/admin/admin.module').then( m => m.AdminPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'productos',
    loadChildren: () => import('./pages/productos/productos.module').then( m => m.ProductosPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'clientes',
    loadChildren: () => import('./pages/clientes/clientes.module').then( m => m.ClientesPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'aproducto',
    loadChildren: () => import('./pages/aproducto/aproducto.module').then( m => m.AproductoPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'acliente',
    loadChildren: () => import('./pages/acliente/acliente.module').then( m => m.AclientePageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'ecliente/:id',
    loadChildren: () => import('./pages/ecliente/ecliente.module').then( m => m.EclientePageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'eproducto',
    loadChildren: () => import('./pages/eproducto/eproducto.module').then( m => m.EproductoPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'cproductos',
    loadChildren: () => import('./pages/cproductos/cproductos.module').then( m => m.CproductosPageModule),
    canLoad: [AuthGuard]
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'mapacliente',
    loadChildren: () => import('./pages/mapacliente/mapacliente.module').then( m => m.MapaclientePageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
